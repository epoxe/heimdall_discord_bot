import discord
from discord.ext import commands
from discord.utils import get
import config
import os
import io
import random
from datetime import datetime
from discord import Webhook, AsyncWebhookAdapter
from profile import * #Imports the profile.py file and all functions

#example command for reference
 
@commands.command()
async def chrisroberts(ctx, *, question = None):
    """
    Ask Chris a question.

    Example: !chrisroberts Will you ever finish Squadron 42?
    
    Parameters
    ----------
        QUESTION
        Ask Chris your question.

    Returns
    -------
        Chris Robert's actual response, straight from the big man.
    """
    answers = random.randint(1,8)

    if question == None:
        await ctx.send('You have to ask a question, dumb ass.')

    elif answers == 1:
        cr_answer = "It is certain"
    
    elif answers == 2:
        cr_answer = "Outlook good"
    
    elif answers == 3:
        cr_answer = "You may rely on it"
    
    elif answers == 4:
        cr_answer = "Ask again later"
    
    elif answers == 5:
        cr_answer = "Concentrate and ask again"
    
    elif answers == 6:
        cr_answer = "Reply hazy, try again"
    
    elif answers == 7:
        cr_answer = "My reply is no"
    
    elif answers == 8:
        cr_answer = "My sources say no"

    cr_responds = f"Chris Roberts says '{cr_answer}'"
    await ctx.send(cr_responds)

def setup(bot): #register commands with the discord bot.
    bot.add_command(chrisroberts)