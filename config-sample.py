import os

# This is set up to come from an environment variable. Set the HEIMDALL_BOT_TOKEN environment
# variable to the discord bot token and then run the bot. This will pick it up.
# This is good for docker too, if you use that. Or just set it to your token directly.
#token = os.environ['HEIMDALL_BOT_TOKEN']

AIRTABLE_BASE_ID_1 = 'xxxxxxxxxxxxxxxxxxx'
AIRTABLE_BASE_ID_2 = ''
AIRTABLE_API_KEY = 'xxxxxxxxxxxxxxxx'
BASE_URL = ''
WEBHOOK_URL = ''

token = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
SERVER_ID = 111111111111111
DISCORD_GUILD = "Heimdall Test"

CHANNEL_GENERAL = 1111111111111111111111111 #is .id for discord channel general

#dont forget to goto https://discord.com/developers/applications/BOTIDHERE/oauth2/url-generator and invite your bot!