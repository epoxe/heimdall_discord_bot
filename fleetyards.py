from helpers import * #Imports the helpers.py file and all functions
import requests
import json
from datetime import datetime
import config
import discord
import os
import re
from discord.ext import commands
from discord.utils import get
from datetime import datetime
from discord import Webhook, AsyncWebhookAdapter

# FLEETYARDS.PY
# CONTAINS BOT FUNCTIONS THAT PERTAIN TO FLEETYARDS:
# Bot Command: !fleetsync

#When user types !fy-sync FleetyardsUsername, function deletes their existing ships and replaces them with an imported list of ships from Fleetyards
@commands.command()
async def syncfleet(ctx, *, fleetyards_username = None):
    """
    Updates your fleet from Fleetyards.net hangar.

    Example: !syncfleet NegStatus
    
    Parameters
    ----------
        FLEETYARDS_USERNAME
        Your registered Fleetyards.net user name.

    Returns
    -------
        Will clear your list of ships and get a new
        updated hangar list from Fleetyards.net
    """
    try:
        author_name = ctx.author.display_name

        #Pull data from the public Fleetyards hangar...

        if fleetyards_username == None:
            await ctx.send("You have to specify a Fleetyards.net username with this command. !help syncfleet for more information.")
        else:
            fleetyards_request = requests.get('https://api.fleetyards.net/v1/vehicles/' + fleetyards_username)

            #Establishing variables
            member_lookup = author_name
            member_lookup_col = "guilded_alias"

            #We'll search for an Airtable member using the specified member name again the specified lookup column...
            member = {} #Set member as a dictionary...
            airtable_member = airtable_members.search(member_lookup_col, member_lookup)

            #This condition happens when no Airtable Member record is found...
            if airtable_member == []:
                member_lookup_col = "rsi_handle"
                airtable_member = airtable_members.get_all(formula="FIND('{1}', LOWER({0}))".format(member_lookup_col, member_lookup))
                if airtable_member == []:
                    member_lookup_col = "callsign"
                    airtable_member = airtable_members.get_all(formula="FIND('{1}', LOWER({0}))".format(member_lookup_col, member_lookup))
                    if airtable_member == []:
                        error_message = author_name + ': could not find an entry on Heimdall with your discord alias, callsign, or RSI handle.'
                        await ctx.send(error_message)
            if airtable_member != []:
                for a in airtable_member:
                    member_id = a['id']
                    for b, c in a.items():
                        if b == "fields":
                            member = c

                #Update fleetyards_username in Airtable
                airtable_members.update(str(member_id),{'fleetyards_username':str(fleetyards_username)})

                #Set the NorseLine callsign...
                member_callsign = "Unknown"
                if 'callsign' in member.keys():
                    member_callsign = member['callsign']

                #Delete existing ships belonging to member id from Airtable...
                if 'member_ships' in member.keys():
                    member_ship_ids = sorted(member['member_ships'] ,  key=lambda x: x[1])
                    fleetyards_json = []
                    for a in member_ship_ids:
                        airtable_member_ships.delete(a);

            #For each ship found on Fleetyards, insert a new record in the Airtable 'member ships' base...
            ship_import_count = 0;
            last_id = ""
            ship_inflect = "ships"
            for a in fleetyards_request.json():
                fleetyard_ship_data = airtable_ships.search('ship_export_code', a['model']['slug'])
                for b in fleetyard_ship_data:
                    fleetyard_ship_id = b['id']
                fleetyard_ship_nickname = ""
                if 'name' in a.keys():
                    fleetyard_ship_nickname = a['name']
                if fleetyard_ship_id != last_id:
                    airtable_member_ships.insert({'ship_nickname': fleetyard_ship_nickname, 'member_id': [member_id], 'ship_id': [fleetyard_ship_id]})
                    ship_import_count += 1
                last_id = fleetyard_ship_id
            if ship_import_count == 1:
                ship_inflect = "ship"

            send_message = author_name + ": " + str(ship_import_count) + " " + ship_inflect + " synced with Heimdall from Fleetyards."
            await ctx.send(send_message)
        #If there's an issue, print it to console and let the user know that it failed.
    except Exception as x:
        print (x)
        send_except = ctx.author.display_name + ": something went wrong with your sync. See Vulcan to troubleshoot."
        await ctx.send(send_except)

        

def setup(bot): #register commands with the discord bot.
    bot.add_command(syncfleet)
