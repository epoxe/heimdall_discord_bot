import discord
from discord.ext import commands
from discord import Webhook, AsyncWebhookAdapter
import profile
from helpers import *

# SHIPS.PY
# CONTAINS BOT FUNCTIONS THAT PERTAIN TO SHIPS:
# Bot Command: !ships
# Bot Command: !whohas

@commands.command()
async def ships(ctx, *, search_name = None):
    """
    Reports requested users list of registered ships.

    Example: !ships vulcan
    
    Parameters
    ----------
        SEARCH_NAME (optional)
        If no user is selected the request author will
        be queried.

    Returns
    -------
        The queried users list of ships.
    """
    if search_name is None:
        search_name = ctx.message.author.display_name

    member_lookup = "" #The member name that'll be used to search Airtable
    member_lookup_col = "" #The Airtable column that'll be searched
    footer_message = "" #The message we put in the embed footer (it varies between !ships and !ships Username)

    member_lookup = search_name.lower()
    member_lookup_col = "guilded_alias"
    footer_message = "Ship list requested by " + ctx.author.display_name.capitalize()

    #We'll search for an Airtable member using the specified member name again the specified lookup column...
    member = {} #Set member as a dictionary...
    #airtable_member = airtable_members.search(member_lookup_col, member_lookup)
    airtable_member = airtable_members.get_all(formula="FIND('{1}', LOWER({0}))".format(member_lookup_col, member_lookup))
    if airtable_member == []:
        member_lookup_col = "rsi_handle"
        airtable_member = airtable_members.get_all(formula="FIND('{1}', LOWER({0}))".format(member_lookup_col, member_lookup))
        if airtable_member == []:
            member_lookup_col = "callsign"
            airtable_member = airtable_members.get_all(formula="FIND('{1}', LOWER({0}))".format(member_lookup_col, member_lookup))
            if airtable_member == []:
                error_message = author_name + ': could not find any member with your discord alias, callsign, or RSI handle.'
                await ctx.send(error_message)

    #The 'Else' condition happens when an Airtable Member record is found...
    if airtable_member != []:
        for a in airtable_member:
            for b, c in a.items():
                if b == "fields":
                    member = c

        #Set the NorseLine callsign...
        member_callsign = "Unknown"
        if 'callsign' in member.keys():
            member_callsign = member['callsign']

        #If ships are found, pull info from Airtable for each of the ship IDs, and buid a list string that can be used for the embed...
        if 'member_ships' in member.keys():
            member_ship_ids = sorted(member['member_ships'] ,  key=lambda x: x[1])
            member_ships = ""
            ship_count = 0
            ship_inflect = "Ships"
            for a in member_ship_ids:
                ship_count += 1
                airtable_member_ships_list = airtable_member_ships.get(a);
                airtable_ship_details = airtable_ships.get(str(airtable_member_ships_list['fields']['ship_id'][0]));
                member_ships += "`› " + airtable_ship_details['fields']['ship_manufacturer_code'] + " "
                member_ships += airtable_ship_details['fields']['ship_model'] + " "
                member_ships_nickname = ""
                if 'ship_nickname' in airtable_member_ships_list['fields'].keys():
                    member_ships_nickname = '"' + airtable_member_ships_list['fields']['ship_nickname'] + '"'
                member_ships += member_ships_nickname + '`\n'
            if ship_count == 1:
                ship_inflect = "Ship"
        #If no ratings are found, prepare a "no ratings found" message...
        else:
            member_ships = ""
            ship_count = 0
            ship_inflect = "Ships"
            ratings = "_No registered ships_"
            await ctx.send("No ships registered. Talk to vulcan to get added to Heimdall, then use the !syncfleet command.")

        embed = discord.Embed(title=member_callsign.upper() + "'S SHIP LIST",color=0x376799)
        embed.add_field(name=str(ship_count) + " " + ship_inflect, value=member_ships)
        embed.set_footer(text=footer_message)
        await ctx.send(embed=embed)

# When user types !whohas ShipKeyword, function returns a list of matching ships and owners
@commands.command()
async def whohas(ctx, *, ship_name = None):
    """
    Reports which users have a specific ship.

    Example: !whohas Vanguard
    
    Parameters
    ----------
        SHIP_NAME
        Which ship the user would like to search.

    Returns
    -------
        The  queried list of users that own the specified ship.
    """

    if ship_name == None: #Check if the command is for self
        error_message = ctx.author.display_name + ': no valid ship listed'
        await ctx.send(error_message)
    else:
        #We'll search for an Airtable ship record using the provided keyword...
        #This API search method looks for a complete string match BUT looks for both uppercase and lowercase matches (ie, the ROC)...
        airtable_ships_list = airtable_ships.get_all(formula="OR({ship_model}='" + ship_name.capitalize() + "',{ship_model}='" + ship_name.upper() + "')")

        #If the first query fails, use an alternate lookup method, which capitalizes the string (ie, cutlass to Cutlass) and looks for a partial match...
        if airtable_ships_list == []:
            airtable_ships_list = airtable_ships.get_all(formula="FIND('" + ship_name.capitalize() + "',{ship_model}) > 0")

        #If after two API calls, the ship list is still blank, deliver the sad news...
        error_message = ""
        if airtable_ships_list == []:
            embed = discord.Embed(title="WHO HAS '" + ship_name.upper() + "'?",color=0x376799)
            embed.add_field(name= "Ships Found:", value="None")
            await ctx.send(embed=embed)
        #Loop through each member ship ID that's attached to the ship record(s)...
        else:
            ship_count = 0
            owned_ships = ""
            ship_inflect = "ships"
            for a in airtable_ships_list:
                if 'member_ship_id' in a['fields'].keys():
                    for b in a['fields']['member_ship_id']:
                        ship_count += 1
                        member_ship = airtable_member_ships.get(str(b))
                        #Build a list of ship models and owners
                        owned_ships += str("`› " + str(a['fields']['ship_manufacturer_code']) + " " + str(a['fields']['ship_model']) + " (" + str(member_ship['fields']['member_callsign'][0]) + ")`\n")
            if ship_count == 1:
                ship_inflect = "ship"

            if ship_count > 0:
                embed = discord.Embed(title="WHO HAS '" + ship_name.upper() + "'?",color=0x376799)
                embed.add_field(name=str(ship_count) + " " + ship_inflect + " found:", value=owned_ships)
                await ctx.send(embed=embed)
            else:
                embed = discord.Embed(title="WHO HAS '" + ship_name.upper() + "'?",color=0x376799)
                embed.add_field(name= "Ships Found:", value="None")
                await ctx.send(embed=embed)

            

def setup(bot): #register commands with the discord bot.
    bot.add_command(ships)
    bot.add_command(whohas)

