from helpers import * #Imports the helpers.py file and all functions
import requests
import json
from PIL import Image
from datetime import datetime
import config
import io
import math
import discord
from discord.ext import commands
from datetime import datetime

# PROFILE.PY
# CONTAINS BOT FUNCTIONS THAT PERTAIN TO PROFILE:
# displayUniform(callsign, gender, rank, country, ribbons, medals, ratings, roles, union)
# Bot Command: !profile

def displayUniform(callsign, gender = "male", rank = "prospect", country = "US", ribbons = [], medals = [], ratings = [], roles = [], union = ""):

    #Converts rank name to rank code for image naming convention
    uniform_rank = rank
    if rank.lower() == "prospect":
        uniform_rank = "prospect"
    elif rank.lower() == "crewman":
        uniform_rank = "R0"
    elif rank.lower() == "advanced crewman":
        uniform_rank = "R1"
    elif rank.lower() == "master crewman":
        uniform_rank = "R2"
    elif rank.lower() == "officer":
        uniform_rank = "R3"
    elif rank.lower() == "executive officer" or rank.lower() == "chief operations officer" or rank.lower() == "head of transportation":
        uniform_rank = "R4"
    elif rank.lower() == "chairman & ceo" or rank.lower() == "chairman":
        uniform_rank = "R5"

    gender_suffix = ""
    if gender.lower() == "female":
        gender_suffix = "_female"

    base_uniform = Image.open("./uniform/" + uniform_rank + gender_suffix + ".png")

    flair_country = Image.open("./uniform/country-" + country + ".png")
    base_uniform.paste(flair_country, (261, 195), flair_country)

    rope_color = ""

    if uniform_rank != "R4" and uniform_rank != "R5":

        if "green" in roles:
            rope_color = "green"
        if "blue" in roles:
            rope_color = "blue"
        if "orange" in roles:
            rope_color = "orange"
        if "black" in roles:
            rope_color = "black"
        if "white" in roles:
            rope_color = "white"
        if "red" in roles:
            rope_color = "red"

    if rope_color != "":
        rope_image = Image.open("./uniform/rope_" + rope_color + gender_suffix + ".png")
        base_uniform.paste(rope_image, (642,120), rope_image)

    name_image_list=[]
    name_image_list.append(Image.open("./uniform/name_left.png"))
    for letter in list(callsign.upper()):
        chosen = "./uniform/name_" + letter + ".png"
        name_image_list.append(Image.open(chosen))
    name_image_list.append(Image.open("./uniform/name_right.png"))

    name_count = len(name_image_list)

    name_total_width = 0
    for name_image in name_image_list:
        name_total_width += name_image.width

    name_combo_image = Image.new('RGBA', (name_total_width, (name_image_list[0].height)), (50, 52, 61, 0)) #The last 4 digits here are RGBA color codes...

    x=0
    y=1
    z=0
    for name_image in name_image_list:
        new_image=name_image_list[x]
        prev_image=name_image_list[z]
        if y == 1:
            x_pos = 0
        else:
            x_pos += (prev_image.width)
        y_pos = 0
        name_combo_image.paste(name_image_list[x], (x_pos, y_pos))
        x += 1
        y += 1
        z += 1
        if x == 1:
            z = 0

    name_offset = 280 - (((name_image_list[0].width*name_count) - name_image_list[0].width)/2)

    base_uniform.paste(name_combo_image, (int(name_offset), 239), name_combo_image)

    if ribbons != []:
        ribbon_image_list=[]
        for ribbon in ribbons:
            chosen = "./uniform/" + ribbon + ".png"
            ribbon_image_list.append(Image.open(chosen))

        ribbon_count = len(ribbon_image_list)

        ribbon_row_count = math.ceil(ribbon_count/4)

        ribbon_combo_image = Image.new('RGBA', ((ribbon_image_list[0].width*4), (ribbon_image_list[0].height*ribbon_row_count)), (50, 52, 61, 0)) #The last 4 digits here are RGBA color codes...

        x=0
        y=1
        row=0
        for ribbon_image in ribbon_image_list:
            new_image=ribbon_image_list[x]
            if y == 1:
                x_pos = 0
            else:
                x_pos += (new_image.width)
            y_pos = row*(new_image.height)
            ribbon_combo_image.paste(ribbon_image_list[x], (x_pos, y_pos))
            x += 1
            y += 1
            if (y / 4) > 1:
                row += 1
                y = 1

        if ribbon_count > 4:
            ribbon_count = 4
        ribbon_offset = 510 - (((ribbon_image_list[0].width*ribbon_count) - ribbon_image_list[0].width)/2)

        base_uniform.paste(ribbon_combo_image, (int(ribbon_offset), 243), ribbon_combo_image)

    else:
        ribbon_combo_image = 0

    if medals != []:
        medal_image_list=[]
        for medal in medals:
            chosen = "./uniform/" + medal + ".png"
            medal_image_list.append(Image.open(chosen))

        medal_count = len(medal_image_list)

        medal_row_count = math.ceil(medal_count/3)

        medal_combo_image = Image.new('RGBA', ((medal_image_list[0].width*3), (medal_image_list[0].height*medal_row_count)), (50, 52, 61, 0)) #The last 4 digits here are RGBA color codes...

        x=0
        y=1
        row=0
        for medal_image in medal_image_list:
            new_image=medal_image_list[x]
            if y == 1:
                x_pos = 0
            else:
                x_pos += (new_image.width)
            y_pos = row*(new_image.height)
            medal_combo_image.paste(medal_image_list[x], (x_pos, y_pos))
            x += 1
            y += 1
            if (y / 3) > 1:
                row += 1
                y = 1

        if medal_count > 3:
            medal_count = 3
        medal_offset = 506 - (((medal_image_list[0].width*medal_count) - medal_image_list[0].width)/2)
        medal_vert_offset = 0
        if ribbon_combo_image != 0:
            medal_vert_offset = ribbon_combo_image.height + 15

        base_uniform.paste(medal_combo_image, (int(medal_offset), int(243 + medal_vert_offset)), medal_combo_image)

    if ratings != []:
        ratings_image_list=[]
        for rating in ratings:
            chosen = "./uniform/" + rating + ".png"
            ratings_image_list.append(Image.open(chosen))

        ratings_count = len(ratings_image_list)

        ratings_row_count = math.ceil(ratings_count/3)

        ratings_combo_image = Image.new('RGBA', ((ratings_image_list[0].width*3), (ratings_image_list[0].height*ratings_row_count)), (50, 52, 61, 0)) #The last 4 digits here are RGBA color codes...

        x=0
        y=1
        row=0
        for rating_image in ratings_image_list:
            new_image=ratings_image_list[x]
            if y == 1:
                x_pos = 0
            else:
                x_pos += (new_image.width)
            y_pos = row*(new_image.height)
            ratings_combo_image.paste(ratings_image_list[x], (x_pos, y_pos))
            x += 1
            y += 1
            if (y / 3) > 1:
                row += 1
                y = 1

        if ratings_count > 3:
            ratings_count = 3
        ratings_offset = 265 - (((ratings_image_list[0].width*ratings_count) - ratings_image_list[0].width)/2)

        base_uniform.paste(ratings_combo_image, (int(ratings_offset), 280), ratings_combo_image)

    else:
        ratings_combo_image = 0

    if union != "" and uniform_rank != "R5" and uniform_rank != "R4" and uniform_rank != "R3" and uniform_rank != "prospect":
        pin_vert_offset = 0

        if ratings_combo_image != 0:
            pin_vert_offset = ratings_combo_image.height + 15

        union_pin = Image.open("./uniform/union-pin.png")
        base_uniform.paste(union_pin, (264, int(283 + pin_vert_offset)), union_pin)

    base_uniform.save(('./static/uniform-' + callsign + '.png'),'PNG')

    return "Cool"

@commands.command()
async def profile(ctx, *, search_name = None):
    """
    Reports requested users profile.

    Example: !profile vulcan
    
    Parameters
    ----------
        SEARCH_NAME (optional)
        If no user is selected the request author will
        be queried.

    Returns
    -------
        The queried users profile and uniform.
    """

    #Establishing variables
    member_lookup = "" #The member name that'll be used to search Airtable
    member_lookup_col = "" #The Airtable column that'll be searched
    footer_message = "" #The message we put in the embed footer (it varies between !profile and !profile Username)
    ribbon_list = []
    medal_list = []
    ratings_list = []
    roles_list = []
    author_name = ctx.author.display_name # member who requested the command
   
    #This condition happens when the search_name argument is not specified (ie, "!profile")...
    if search_name == None:
        member_lookup = author_name.lower()
        member_lookup_col = "guilded_alias"
        footer_message = "To post your own, try !profile"

    #This condition happens when the search_name argument is specified (ie, "!profile Username")...
    else:
        member_lookup = search_name.lower()
        member_lookup_col = "guilded_alias"
        footer_message = "Profile requested by " + author_name

    #We'll search for an Airtable member using the specified member name again the specified lookup column...
    member = {} #Set member as a dictionary...
    airtable_member = airtable_members.get_all(formula="FIND('{1}', LOWER({0}))".format(member_lookup_col, member_lookup))

    #This condition happens when no Airtable Member record is found...
    error_message = ""
    if airtable_member == []:
        member_lookup_col = "rsi_handle"
        airtable_member = airtable_members.get_all(formula="FIND('{1}', LOWER({0}))".format(member_lookup_col, member_lookup))
        if airtable_member == []:
            member_lookup_col = "callsign"
            airtable_member = airtable_members.get_all(formula="FIND('{1}', LOWER({0}))".format(member_lookup_col, member_lookup))
            if airtable_member == []:
                error_message = author_name + ': could not find a profile with your discord alias, callsign, or RSI handle.'
                await ctx.send(error_message)
            
    #The 'Else' condition happens when an Airtable Member record is found...
    if airtable_member != []:
        for a in airtable_member:
            for b, c in a.items():
                if b == "fields":
                    member = c

        #Set the NorseLine callsign...
        member_callsign = "Unknown"
        if 'callsign' in member.keys():
            member_callsign = member['callsign']
            print("member callsign {}".format(member_callsign))

        #Count the ships...
        ship_count = 0
        ship_inflect = "ships"
        if 'member_ships' in member.keys():
            for a in member['member_ships']:
                ship_count += 1
        if ship_count == 1:
            ship_inflect = "ship"

        #Set Fleetyards username...
        embed_ships = str(ship_count) + " " + ship_inflect
        if 'fleetyards_username' in member.keys():
            embed_ships = "[" + str(ship_count) + " " + ship_inflect + "](https://fleetyards.net/hangar/" + member['fleetyards_username'] +")"

        #Set the RSI handle...
        rsi_handle = "_Unknown_"
        if 'rsi_handle' in member.keys():
            rsi_handle = member['rsi_handle']

        #Set the rank...
        rank = "_Unknown_"
        if 'rank' in member.keys():
            rank = member['rank']

        #Set the roleplay gender...
        rp_gender = "male"
        if 'rp_gender' in member.keys():
            rp_gender = member['rp_gender']

        #Set the country...
        country = "_Unknown_"
        if 'country' in member.keys():
            country = member['country']

        #Set the country...
        union = ""
        if 'union' in member.keys():
            union = member['union']

        #Set and format the join date...
        join_date = "_Unknown_"
        longago_url = "_Unknown_"
        if 'join_date' in member.keys():
            join_date = '{dt:%B} {dt.day}, {dt.year}'.format(dt = datetime.strptime(member['join_date'], '%Y-%m-%d'))
            longago_format = '{dt.year}/{dt:%B}/{dt.day}'.format(dt = datetime.strptime(member['join_date'], '%Y-%m-%d'))
            longago_url = "[" + join_date + "](https://howlongagogo.com/date/" + longago_format + ")"

        #If awards are found, pull info from Airtable for each of the award IDs listed on the member's Airtable record...
        if 'awards' in member.keys():

            #Set a few variables...
            award_ids = member['awards']
            awards = ""
            award_count = 0

            #Loop through each award ID, grab Airtable details for the award, and build an award list string (for the embed) and an award image list (which'll be combined later)...
            for a in award_ids:
                airtable_award_list = airtable_awards.get(a);
                for b, c in airtable_award_list.items():
                    if b == "fields":
                        awards += "› "
                        awards += c['award']
                        awards += "\n"
                        award_count += 1
                        if c['type'] == 'ribbon':
                            ribbon_list.append(c['file_name'])
                        elif c['type'] == 'medal':
                            medal_list.append(c['file_name'])


        #If no awards are found, prepare a "no awards found" message...
        else:
            awards = "_No awards (yet)_"

        #If ratings are found, pull info from Airtable for each of the rating IDs, and buid a list string that can be used for the embed...
        if 'ratings' in member.keys():
            rating_ids = sorted(member['ratings'] ,  key=lambda x: x[1])
            ratings = ""
            for a in rating_ids:
                airtable_rating_list = airtable_ratings.get(a);
                for b, c in airtable_rating_list.items():
                    if b == "fields":
                        ratings += "› "
                        ratings += c['rating']
                        ratings += "\n"
                        if c['type'] == 'rating':
                            ratings_list.append(c['file_name'])
                        if c['type'] == 'role':
                            roles_list.append(c['file_name'])

        #If no ratings are found, prepare a "no ratings found" message...
        else:
            ratings = "_No roles or ratings (yet)_"

        #Generate uniform
        displayUniform(member_callsign, rp_gender, rank, country, ribbon_list, medal_list, ratings_list, roles_list, union)

        #Format profile embed and send it.
        file = discord.File("./static/uniform-" + member_callsign + ".png", filename="image.png")
        embed = discord.Embed(title="[" + member_lookup.capitalize() + "'s Norseline Profile]",color=0x376799)
        embed.set_image(url="attachment://image.png")
        embed.add_field(name="Rank", value=rank, inline=True)
        embed.add_field(name="Join Date", value=longago_url, inline=True)
        embed.add_field(name="RSI Handle", value="[" + rsi_handle + "](https://robertsspaceindustries.com/citizens/" + rsi_handle + ")", inline=True)
        embed.add_field(name="Ships", value=embed_ships, inline=True)
        embed.add_field(name="Roles & Ratings", value=ratings, inline=False)
        embed.add_field(name="Awards", value=awards, inline=False)
        embed.set_footer(text=footer_message)
        await ctx.send(file=file, embed=embed)

def setup(bot): #register commands with the discord bot.
    bot.add_command(profile)