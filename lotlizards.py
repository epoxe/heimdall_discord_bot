from helpers import * #Imports the helpers.py file and all functions
import requests
import json
from PIL import Image
from datetime import datetime
import config
import io
import math

# LOTLIZARDS.PY
# CONTAINS BOT FUNCTIONS THAT PERTAIN TO LOT LIZARDING:
# # displayLizard()

def displayLizard(handle, outfit_id = "1",rank = "R0"):

    base_outfit = Image.open("./uniform/lizards/outfit_" + str(outfit_id) + ".png")

    # OUTFIT 1 PARAMS
    if str(outfit_id) == "1":
        outfit_params = {
            "name_id": 1,
            "name_x": 270,
            "name_y": 219,
            "name_rotate": 5,
            "name_scale": .85,
            "patch_id": 1,
            "patch_x": 503,
            "patch_y": 202,
            "patch_rotate": -5,
            "patch_scale": 1
        }

    # OUTFIT 2 PARAMS
    if str(outfit_id) == "2":
        outfit_params = {
            "name_id": 1,
            "name_x": 308,
            "name_y": 237,
            "name_rotate": 8,
            "name_scale": .8,
            "patch_id": 1,
            "patch_x": 495,
            "patch_y": 83,
            "patch_rotate": -19,
            "patch_scale": .9
        }

    # ADDS NAMEBADGE TO THE OUTFIT
    lizardName(handle, outfit_params, base_outfit)

    # ADDS RANK & PATCH TO THE OUTFIT
    lizardRank(rank, outfit_params, base_outfit)

    base_outfit.save(('./static/lizard-' + handle + '.png'),'PNG')

    return "Cool"

def lizardName(handle, params, base_outfit):
    name_image_list=[]
    name_image_list.append(Image.open("./uniform/lizards/" + str(params["name_id"]) + "_name_left.png"))
    for letter in list(handle.upper()):
        chosen = "./uniform/lizards/" + str(params["name_id"]) + "_name_" + letter + ".png"
        name_image_list.append(Image.open(chosen))
    name_image_list.append(Image.open("./uniform/lizards/" + str(params["name_id"]) + "_name_right.png"))

    name_count = len(name_image_list)

    name_total_width = 0
    for name_image in name_image_list:
        name_total_width += name_image.width

    name_combo_image = Image.new('RGBA', (int(2 + name_total_width), int(2 + (name_image_list[0].height))), (50, 52, 61, 0))

    x=0
    y=1
    z=0
    for name_image in name_image_list:
        new_image=name_image_list[x]
        prev_image=name_image_list[z]
        if y == 1:
            x_pos = 1
        else:
            x_pos += (prev_image.width)
        y_pos = 1
        name_combo_image.paste(name_image_list[x], (x_pos, y_pos))
        x += 1
        y += 1
        z += 1
        if x == 1:
            z = 0

    name_offset = params["name_x"] - (((name_image_list[0].width*name_count) - name_image_list[0].width)/2)

    name_combo_image = name_combo_image.resize((int(name_combo_image.width * params["name_scale"]),int(name_combo_image.height * params["name_scale"])), resample=Image.LANCZOS)

    name_combo_image = name_combo_image.rotate(params["name_rotate"], resample=Image.BICUBIC, expand=True)

    base_outfit.paste(name_combo_image, (int(name_offset), params["name_y"]), name_combo_image)

    return "Success"

def lizardRank(rank, params, base_outfit):

    uniform_rank = rank
    if rank.lower() == "guest":
        uniform_rank = "guest"
    elif rank.lower() == "hatchling":
        uniform_rank = "R0"
    elif rank.lower() == "horny toad":
        uniform_rank = "R1"
    elif rank.lower() == "iguana":
        uniform_rank = "R2"
    elif rank.lower() == "gila monster":
        uniform_rank = "R3"
    elif rank.lower() == "monitor":
        uniform_rank = "R4"
    elif rank.lower() == "komodo":
        uniform_rank = "R5"

    if uniform_rank != "guest":
        patch_image = Image.open("./uniform/lizards/" + str(params["patch_id"]) + "_lotlizard-patch.png")
        patch_image = patch_image.resize((int(patch_image.width * params["patch_scale"]),int(patch_image.height * params["patch_scale"])), resample=Image.LANCZOS)
        patch_image = patch_image.rotate(params["patch_rotate"], resample=Image.BICUBIC, expand=True)
        base_outfit.paste(patch_image, (params["patch_x"], params["patch_y"]), patch_image)

    return "Success"
