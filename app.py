import config
import ships
import helpers
import profile
import fleetyards
import heimdall
import discord
from PIL import Image
from discord.ext import commands
from discord.utils import get

#initialize discord bot
bot = commands.Bot(
    command_prefix='!',
    description='The NorseLine gate keeper, and knower of things.',
    case_insensitive=True
    )

#announce the bot is online
@bot.event
async def on_ready():
    print('Heimdall is online and ready.')

bot.load_extension("heimdall") # Extension to the full list of heimdall commands
bot.load_extension("ships") # Extension to the full list of ship commands
bot.load_extension("profile") # Extension to the full list of profile commands
bot.load_extension("fleetyards") # Extension to the full list of fleetyard commands

# Run the bot
bot.run(config.token)
