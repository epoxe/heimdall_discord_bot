# Packages Used

[Airtable Python Wrapper](https://airtable-python-wrapper.readthedocs.io/): The Airtable Python Wrapper is one of the main non-default libraries used to make stuff work.

[PIL / Pillow](https://pillow.readthedocs.io/): PIL / Pillow is used for image processing for medals.
