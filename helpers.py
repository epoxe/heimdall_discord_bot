from airtable import Airtable
import config

# HELPERS.PY
# CONTAINS HELPFUL, REUSABLE SNIPPETS SHARED BY MULTIPLE FUNCTIONS

#Airtable API calls for Base 1 (Members, Ships, Member Ships, Ratings, Awards)
airtable_members = Airtable(config.AIRTABLE_BASE_ID_1, 'members', config.AIRTABLE_API_KEY)
airtable_awards = Airtable(config.AIRTABLE_BASE_ID_1, 'awards', config.AIRTABLE_API_KEY)
airtable_ratings = Airtable(config.AIRTABLE_BASE_ID_1, 'ratings', config.AIRTABLE_API_KEY)
airtable_member_ships = Airtable(config.AIRTABLE_BASE_ID_1, 'member_ships', config.AIRTABLE_API_KEY)
airtable_ships = Airtable(config.AIRTABLE_BASE_ID_1, 'ships', config.AIRTABLE_API_KEY)

#Airtable API calls for Base 2 (Citizens, Orgs)
airtable_citizens = Airtable(config.AIRTABLE_BASE_ID_2, 'citizens', config.AIRTABLE_API_KEY)

#Set webhook address
webhook_url = config.WEBHOOK_URL
